# An opinionated build system for the Iowa City community mesh network

This build system uses [Astro's nix-openwrt-imagebuilder](https://github.com/astro/nix-openwrt-imagebuilder) and reuses the NixOS [configuration module system](https://nixos.org/manual/nixos/stable/#sec-configuration-syntax).

Common configuration is in [modules](./modules) and hardware specific configuration is in [flake.nix](./flake.nix).

Automated builds available here: https://hydra.hq.c3d2.de/project/meshic

## Building

You'll need to [install Nix](https://nixos.org/download.html) and [enable flakes](https://nixos.wiki/wiki/Flakes).

To build one of the images in [flake.nix](./flake.nix):
```sh
nix build .#<some-router-package in flake.nix>
```

To build all of the images:
```sh
nix build .#
```

## Flashing and testing

Routers without Openwrt would need to be flashed with a "factory" images and routers that already have Openwrt installed need only a "sysupgrade" image.

SSH is available from the WAN port with these builds so after flashing I recommend connecting router WAN ports to a switch and SSH to them as their IPs show up in the log of you DHCP server.

There is no DHCP server installed in the image so I've also connected the WAN port to a LAN port on the same router, and then chained the LAN ports between routers.
