{
  dropbear.settings.PasswordAuth =
    # Allow login without a password for now.
    true;

  extraFiles."etc/dropbear/authorized_keys".text =
    # add your SSH key here
    ''
      ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqAXBEROEfldkHdUbF3TinBhfeX5l4DQ/5MAOhLh09avqCqcHY2FanZN+qmWpD695UZ71Cl+XF6Bj0KO7Rt4SAemvkEDPMBoidkt+ZjLsdnb8GVvbDhu/62JnqW9meYTN5GcjfmKMPDtKFbgSx9PPcjsDaO6LI/GWeyTz+EYQqwTdc7TKffjLXp6bREYLf0oKIBTvW9/oPCBI7ywYmyBaadFKrYSnujJbMejH91L+JN2fJoxjjhsGcRR78ottbjz4q6JxYjt9CG5oa7Lm60xdZkiiA3c4dMuHU9+EWGshjBKL1Fb9BafeAKhHobcs7UG8IVlqHRJC5VAGQlmus/fNagAArz9PnGW4MAOgg+yLjQJLLKqePBMsAsMHZ9XT+sqPyJfcai5dWynGXFP1B63C/oosVMkeZAlBIwDz/CmufpKBCJZXCfFoC3PotWsH/JT3ir/RSdtVHQ169CEgm+AUd7gjBuRwn6j2eBHcYkn0nCbQ93KLPlnYCLXzjByGved8= emery@zuni
    '';

  extraFiles."etc/uci-defaults/100-firewall".text =
    # allow SSH at the wan port
    ''
      uci -q batch << EOB
      add firewall rule
      set firewall.@rule[-1].src='wan'
      set firewall.@rule[-1].target='ACCEPT'
      set firewall.@rule[-1].proto='tcp'
      set firewall.@rule[-1].dest_port='22'
      commit firewall
      EOB
    '';

}
