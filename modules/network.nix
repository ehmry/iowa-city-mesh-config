{
  packages.include = [ "batctl-default" "kmod-batman-adv" ];

  # https://www.open-mesh.org/projects/batman-adv/wiki/Batman-adv-openwrt-config

  network = {

    device.br_lan.ports = [ "bat0" ];

    interface = {

      bat0 = {
        proto = "batadv";
        routing_algo = "BATMAN_IV";
        aggregated_ogms = 1;
        ap_isolation = 0;
        bonding = 0;
        fragmentation = 1;
        gw_mode = false;
        #gw_bandwidth = "10000/2000";
        #gw_sel_class = 20;
        orig_interval = 1000;
        bridge_loop_avoidance = 1;
        distributed_arp_table = 1;
        multicast_mode = 1;
        multicast_fanout = 16;
        network_coding = 0;
        hop_penalty = 30;
        isolation_mark = "0x00000000/0x00000000";
      };

      bat0_hardif_eth0 = {
        proto = "batadv_hardif";
        master = "bat0";
        mtu = 1536;
        fname = "eth0";
        hop_penalty = 15;
        throughput_override = 0;
      };

      bat0_hardif_mesh0 = {
        proto = "batadv_hardif";
        master = "bat0";
        mtu = 1536;
        # option ifname is filled out by the wifi-iface
      };

    };
  };

  extraFiles."etc/uci-defaults/13-fix-br-lan".source =
    builtins.toFile "13-fix-br-lan" ''
      # give a name to the thing with a name
      [ "$(uci -q get network.@device[0].name)" = "br-lan" ] &&
        uci -q rename network.@device[0]=br_lan
    '';
}
