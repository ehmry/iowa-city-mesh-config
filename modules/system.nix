{ config, ... }:

{
  system.settings = {
    # Use the hardware profile as a hostname to tell these things apart
    hostname = config.build.profile;
    description = "Iowa City Mesh test router (${config.build.profile})";
    timezone = "CST6CDT,M3.2.0,M11.1.0";
  };

  packages.include = [ "nano" ];
}
