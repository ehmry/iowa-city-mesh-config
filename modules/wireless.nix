{
  wireless = {

    devices = {
      radio0.disabled = false;
      radio1.disabled = false;
    };

    interfaces = let
      apCommon = {
        network = "lan";
        mode = "ap";
        ssid = "meshic";
      };
    in {

      default_radio0 = apCommon // { device = "radio0"; };
      default_radio1 = apCommon // { device = "radio1"; };

      mesh0 = {
        encryption = "none";
        mode = "mesh";
        mesh_id = "strangetractor";
        mesh_fwding = false; # don't use 802.11s routing
        network = "bat0_hardif_mesh0"; # see ./network.nix
        ifname = "mesh0";
        mesh_ttl = 1;
        mcast_rate = 24000;
      };

    };
  };
}
