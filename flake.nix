# See also https://openwrt.org/docs/guide-developer/imagebuilder_frontends

{
  inputs.openwrt-imagebuilder.url =
    "git+https://gitea.c3d2.de/ehmry/nix-openwrt-imagebuilder.git";

  outputs = { self, openwrt-imagebuilder }:
    let
      inherit (openwrt-imagebuilder.inputs) nixpkgs;
      inherit (nixpkgs) lib;
    in {

      inherit (nixpkgs) legacyPackages;

      packages.x86_64-linux = let
        mkImage =
          # build a firmware image with our customizations
          hardwareSpecificModule:
          let
            sys = openwrt-imagebuilder.lib.openwrtSystem {
              pkgs = self.legacyPackages.x86_64-linux;
              modules =
                # add the common modules and hardware customization
                [
                  ./modules/network.nix
                  ./modules/ssh.nix
                  ./modules/system.nix
                  ./modules/wireless.nix
                  hardwareSpecificModule
                ];
            };
          in builtins.trace
          # dump the UCI commands batch for debugging
          sys.config.extraFiles."/etc/uci-defaults/99-config.uci.batch".text
          sys.config.system.build.image;
      in {
        # Add your router here.
        # The hardware profile can be found by looking in the
        # URL for the standard Openwrt images for your device.

        netaidkit = mkImage {
          build.profile = "glinet_6416";
          wireless.interfaces.mesh0.device = "radio0"; # 2Ghz
        };

        tplink = mkImage {
          build.profile = "tplink_archer-a7-v5";
          wireless.interfaces.mesh0.device = "radio1"; # 2Ghz
        };

        trendnet = mkImage {
          build.profile = "trendnet_tew-691gr";
          wireless.interfaces.mesh0.device = "radio0"; # 2Ghz
        };
      };

      defaultPackage.x86_64-linux =
        # combined build of all router images
        self.legacyPackages.x86_64-linux.symlinkJoin {
          name = "all-images";
          paths = builtins.attrValues self.packages.x86_64-linux;
        };

      hydraJobs =
        # Job for automated building
        lib.attrsets.mapAttrs (_: lib.hydraJob) self.packages.x86_64-linux;
    };

}
